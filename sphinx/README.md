# Sphinx RST to HTML Preview

Command for generating previews of RST files on projects.blender.org.

The template is adapted from the Blender manual to support the same extensions.

### Deployment

Install dependencies.

    python3 -m venv venv
    ./venv/bin/pip3 install -r requirements.txt

Add to Gitea app.ini.

    [markup.restructuredtext]
    ENABLED = true
    FILE_EXTENSIONS = .rst
    RENDER_COMMAND = timeout 30s sudo --preserve-env=GITEA_PREFIX_SRC -u sphinx ./custom/sphinx/venv/bin/python3 ./custom/sphinx/sphinx_to_html.py

    [markup.sanitizer.restructuredtext]
    ELEMENT = div
    ALLOW_ATTR = class
    REGEXP = ^(restructuredtext)$

    [repository.editor]
    LINE_WRAP_EXTENSIONS = .txt,.md,.markdown,.mdown,.mkd,.rst
    PREVIEWABLE_FILE_MODES = markdown,restructuredtext

The `sphinx` user is required for sandboxing of sphinx-build which we do not
assume to be so secure that it should have access to all gitea data. This user
needs read-only access to the custom/sphinx, and the user running gitea needs
to be able to run a process as the `sphinx` user.
